---
Week: 46
Content:  Phase two
Material: See links in weekly plan
Initials: RUTR/ILES
---

# Week 46 updated

## Goals of the week(s)
Lining up new goals for Project B. 

Datacenter teams - meet up with Hans and receive instructions from him. You most likely meet in Network Lab.

For the rest please make sure that you meet us in E-Lab

### Practical goals
* Follow Hans plans and instructions
* For the rest of the students: outline new plan.
* Vleppo : Jesper comes from Vleppo and gives instructins for the new phase of the project
* ITIntelligence: New outline plan will be give as it comes in.

### Learning goals
* Next step outline


## Deliverables

Gitlab update project


## Schedule

Below is the tentative schedule, which may be changed depending on input from the students.

### Monday Vleppo meeting at E-Lab

| Time | Activity |
| :---: | :--- |
| 9:00 | All Teams meet at the ELab|
| 09:45| outline new goals Q & A update gitlab work for the rest of the day|

Blockchain Project Part 2.
The current status for what we call Project Sol is that the interest of having a household device capable of monitoring and assisting owners to sell/buy excess green energy is increasing. We currently have receive positive feedback from both American and Australian companies in our current ideas for the project.

Task list:
* Research and development, compile a list of what you would find innovative in this market. Is there some specific sensors, calculations that might improve on a system like this.

* As a group agree on three of the ideas, and try to explore it further. It would be optimal to try and develop a proof of concept to support your project idea. Remember the KISS model!

* Create a small pitch for the idea, that you can present with the included proof of concept for each of them. Remember to try and consider what you as a group would recommend to implement and develop.

* When you have presented and one of the ideas have been approved moved into the development phase.


### Monday ITelligence meeting at E-lab

What can you send to Martin and Nikolaj. Main point that needs to be addressed.

| Time | Activity |
| :---: | :--- |
| 10:00 | All Teams meet at the ELab|
| 10:45| outline new goals Q & A update gitlab work for the rest of the day|

Please note that you will also meet with Nikolaj at 12:00 in the e-lab for short status report and next step planning.

### Monday Data Center meeting at N-Lab

Please make sure you follow Hans Plan

| Time | Activity |
| :---: | :--- |
| 09:00 |All teams|
| 12:15 |End of meeting. Update gitlab|

### End of Day

Students will have everything set up and committed by the end of the day.

## Hands-on time

Students are welcomed to work outside regular hours and work/Develop further on their tasks

## Comments
N/A
