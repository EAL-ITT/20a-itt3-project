---
Week: 40
Content:  milestones allocations
Material: See links in weekly plan
Initials: RUTR/ILES
---

# Week 40 updated

Due to Ilas been on leave. The meetings format will be changed for the next two weeks.

## Goals of the week(s)
Follow the Seminar for internship and application.

Internship status update. I have a meeting with Danish Displays in the morgning. I have two or more internship possitions at ItInteligence and there are up to four possitions with Vleppo.

### Practical goals
* Get assistance for making applications
* Learn how to write and approach job in DK


### Learning goals
* Approaching the Danish Job Market.


## Deliverables

N/A

## Schedule

On the 28. september, Monday In Week 40. At 08:15 and until kl. 10.30. Room will be announced later.
 

## Meetings

Meetings in the 28th are canceled in the morning. We will have a group team meeting where all the students will have Q&A. We meet at 11:00.


### End of Day

Work on projects and milestones.


## Hands-on time

Students are welcomed to work outside regular hours and work/Develop further on their tasks

## Comments
* This is an initial plan, it will be an agile project
* The weekly plans will be updated weekly according to team needs
