---
Week: 47
Content:  Phase two
Material: See links in weekly plan
Initials: RUTR/ILES
---

# Week 47

## Goals of the week(s)
Line up new goals for Project B. 

Datacenter teams - meet up with Hans and receive instructions from him. You most likely meet in Network Lab.

For the rest please make sure that you meet us in E-Lab

### Practical goals
* Follow Hans plans and instructions
* For the rest of the students: follow up on new plan.
* Vleppo : Verification of tasks from Jesper
* ITIntelligence: outline review from meeting with NISI

### Learning goals
* Next step outline


## Deliverables

Gitlab update project


## Schedule

Below is the tentative schedule, which may be changed depending on input from the students.

### Monday Vleppo meeting at E-Lab

| Time | Activity |
| :---: | :--- |
| 10:00 | All Teams meet at the ELab|
| 10:45| outline new goals Q & A update gitlab work for the rest of the day|
| 12:30 |Break and class meeting|


### Monday ITelligence meeting at E-lab

| Time | Activity |
| :---: | :--- |
| 09:00 | All Teams meet at the ELab|
| 09:45| outline new goals Q & A update gitlab work for the rest of the day|
| 12:30 |Break and class meeting|


### Monday Data Center meeting at N-Lab

Please make sure you follow Hans Plan

| Time | Activity |
| :---: | :--- |
| 09:00 |All Data Center Teams: follow Hans Plan|
| 12:30 |Break and class meeting|
| 13:00 |Following meeting with Hans, ILES and NISI|


### Afternoon meetings

Meeting is at the Network Lab


| Time | Activity |
| :---: | :--- |
| 12:30 |Internship and final project coordination and website presentation: All groups all projects|
| 12:45 |Results and evaluation of semester|




### End of Day

Students will have everything set up and committed by the end of the day.

## Hands-on time

Students are welcomed to work outside regular hours and work/Develop further on their tasks

## Comments
N/A
