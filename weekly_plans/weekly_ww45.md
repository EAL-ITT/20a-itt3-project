---
Week: 45
Content:  Phase two
Material: See links in weekly plan
Initials: RUTR/ILES
---

# Week 45 updated

## Goals of the week(s)
Lining up new goals for Project B. 

Datacenter teams - meet up with Hans and receive instructions from him. You most likely meet in Network Lab.

For the rest please make sure that you meet us in E-Lab

### Practical goals
* Follow Hans plans and instructions
* For the rest of the students: outline new plan.
* Vleppo : Jesper comes from Vleppo and gives instructins for the new phase of the project
* ITIntelligence: New outline plan will be give as it comes in.

### Learning goals
* Next step outline


## Deliverables

Gitlab update project


## Schedule

Below is the tentative schedule, which may be changed depending on input from the students.

### Monday Vleppo meeting at E-Lab

| Time | Activity |
| :---: | :--- |
| 9:00 | All Teams meet at the ELab|
| 09:45| outline new goals Q & A update gitlab work for the rest of the day|




### Monday ITelligence meeting at E-lab

What can you send to Martin and Nikolaj. Main point that needs to be addressed.

| Time | Activity |
| :---: | :--- |
| 10:00 | All Teams meet at the ELab|
| 10:45| outline new goals Q & A update gitlab work for the rest of the day|

### Monday Data Center meeting at N-Lab

Please make sure you follow Hans Plan

| Time | Activity |
| :---: | :--- |
| 09:00 |All teams|
| 12:15 |End of meeting. Update gitlab|

### End of Day

Students will have everything set up and committed by the end of the day.

## Hands-on time

Students are welcomed to work outside regular hours and work/Develop further on their tasks

## Comments
N/A
