---
Week: 49
Content:  Last presentation and report assistance
Material: See links in weekly plan
Initials: RUTR/ILES
---

# Week 49 updated

## Goals of the week(s)
Look at your products and work. Give advice on your work and report

### Practical goals
* Focus points of report
* Presnetation of work
* Assistance in product


### Learning goals
* Report Verification


## Deliverables

N/A

## Schedule

Everyone meet and work at UCL. Data Center Team meets at N-lab. ILES comes in the afternoon. Everyone else E-Lab, RUTR is at elab most of the day. If assistance is needed by Data Center Team/s it can be requested.


## Meetings

Meetings are all day event.


### Monday Vleppo meeting at E-Lab

Meet at Elab and we take it from there. Jesper, maybe coming again.


### Monday ITelligence meeting at E-lab

Meet at Elab and we take it from there.


### Monday Data Center meeting at N-Lab

You meet at the N-lan, ILES arrives in the afternoon. If you need help you can contact RUTR.


### End of Day

Students should have a clear understanding of what to do and how to continue.

## Hands-on time

N/A

## Comments
* Project work
