---
Week: 50
Content:  Report Assistance
Material: See links in weekly plan
Initials: RUTR/ILES
---

# Week 50 updated

## Goals of the week(s)
Deliver your report and get ready for the exam.

### Practical goals
* Finish your report and submitte


### Learning goals
* Report Verification


## Deliverables

N/A

## Schedule

RUTR will be in the elab from 08:20. ILES will join around lunch time.


## Meetings

Up to each student/group that needs assistance.



## End of Day

Students will have access to RUTR all day for any type of assistance. Students should submite their report during the day - See Wiseflow for deadline and do it in good time.

## Hands-on time

Via wiseflow - your final report

## Comments
Get ready for the exam
