---
Week: 35
Content:  Project statup
Material: See links in weekly plan
Initials: RUTR/ILES
---

# Week 35

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals
* Forming and officiating Teams
* Listening to Project presentations
* Students will start to get initial ideas of the project and technologies
* Set up of gitlab with goals and milestones

### Learning goals
* New technologies
* Team building
* Allocation of resources

## Deliverables
* Team members list
* Team and project contract
* Git lab project page


## Schedule

The schedule for the first day is given below.

### Monday

| Time | Activity |
| :---: | :--- |
| 09:00 | Meeting in B0.15 |
| 09:30 |  introduction to various projects from the Teams |
| 10:15| forming working groups - first draft|
| 11:00|meetings between teams and supervisors. Auditorium A0.29|
| 11:30|Break|
| 12:00|Welcoming the companies. Auditorium A0.29|
| 12:10|Presentation from Itelligence|
| 12:40|Presentation from Vleppo|
| 13:10|Finalizing Teams|

### End of Day

Students will have everything set up and committed by the end of the day.



## Hands-on time

Students are welcomed to work outside regular hours and work/Develop further on their tasks

## Comments
* This is an initial plan, it will be an agile project
* The weekly plans will be updated weekly according to team needs
