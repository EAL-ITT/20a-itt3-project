---
Week: 36
Content:  Project overview
Material: See links in weekly plan
Initials: RUTR/ILES
---

# Week 36

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals
* Meeting with students and supervisors
* Listening to Project status
* Looking into miles stones
* goals and outcomes

### Learning goals
* Planning
* Team building
* Allocation of resources
* Brain storming

## Deliverables
* Resources and prototype outlook
* Quick and Dirty is the main focus
* Diagrams, and prototype


## Schedule

Below is the tentative schedule, which may be changed depending on input from the students.

### Monday RUTR

| Time | Activity |
| :---: | :--- |
| 8:30 |  Meeting with Group 1|
| 9:30 |  Meeting with Group 2|
| 10:30| Meeting with Group 3|
| 11:30| Meeting with Group 4|


### Monday ILES

| Time | Activity |
| :---: | :--- |
| 8:30 |  Meeting with Group 5|
| 9:30 |  Meeting with Group 6|
| 10:30| Meeting with Group 7|
| 11:30| Meeting with Group 8|


### End of Day

Students will have everything set up and committed by the end of the day.



## Hands-on time

Students are welcomed to work outside regular hours and work/Develop further on their tasks

## Comments
* This is an initial plan, it will be an agile project
* The weekly plans will be updated weekly according to team needs
