---
Week: 41

Content:  milestones allocations
Material: See links in weekly plan
Initials: RUTR/ILES
---

# Week 41 updated

Project overview

## Goals of the week(s)
Review project stattus and updates.

### Practical goals
* Verify status and overview
* Clarifications and suggestions


### Learning goals
* Status valdiation


## Deliverables

N/A

## Schedule

### Monday Vleppo

| Time | Activity |
| :---: | :--- |
| 9:00 |  Meeting with Group 1 | Thomas, Barnabas, Jens and Gytis|
| 9:20 |  Meeting with Group 2| Victoria, Yani, Daniel, Anton, Ivo|
| 9:40|  Meeting with Group 3| Rune and Rasmus|


### Monday ITelligence

What can you send to Martin and Nikolaj. Main point that needs to be addressed.

| Time | Activity |
| :---: | :--- |
| 10:00 |  Meeting with Group 1| Fadi Al-Salem Marius Tvarkunas Adam Dolinajec Papp Domonkos|
| 10:20 |  Meeting with Group 2| Adham Ahmad, Camilla Johannsen Kock, Jacob Back Ipsen, Jonas Nissen, Martin Banov |


### Monday Data Center

| Time | Activity |
| :---: | :--- |
| 10:40 |  Meeting with Group 1| Ventilation |
| 11:00 |  Meeting with Group 2| Power/Electrical|
| 11:20| Meeting with Group 3| Cool/Heating|


### End of Day

Work on projects and milestones.


## Hands-on time

Students are welcomed to work outside regular hours and work/Develop further on their tasks

## Comments
* This is an initial plan, it will be an agile project
* The weekly plans will be updated weekly according to team needs
