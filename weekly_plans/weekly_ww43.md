---
Week: 43
Content:  milestones allocations
Material: See links in weekly plan
Initials: RUTR/ILES
---

# Week 43 updated

## Goals of the week(s)
Follow the Seminar for internship and application.

### Practical goals
* Get assistance for making applications
* Learn how to write and approach job in DK


### Learning goals
* Approaching the Danish Job Market.


## Deliverables

N/A

## Schedule

On the 19. October, Monday In Week 43. At 08:15 and until kl. 10.30. Room will be announced later.


## Meetings

Helle Thastum the new educational leader will come to class at 11:30. The topic will be the electice course of network automation. Please attent to the class.


### End of Day

Students will have everything set up and committed by the end of the day.

## Hands-on time

Students are welcomed to work outside regular hours and work/Develop further on their tasks

## Comments
* This is an initial plan, it will be an agile project
* The weekly plans will be updated weekly according to team needs
