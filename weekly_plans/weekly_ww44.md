---
Week: 44
Content:  milestones allocations
Material: See links in weekly plan
Initials: RUTR/ILES
---

# Week 44 updated

## Goals of the week(s)
Rounding up questions. Setting new goals per team.

### Practical goals
* Set new goals per team
* Outline exceptions
* Understand the projects new goals and set up gitlab


### Learning goals
* Next step after a successful or failed project.


## Deliverables

Gitlab


## Schedule

Below is the tentative schedule, which may be changed depending on input from the students.

### Monday Vleppo meeting at E-Lab

| Time | Activity |
| :---: | :--- |
| 9:00 |  Meeting with Group 1 | Thomas, Barnabas, Jens and Gytis|
| 9:20 |  Meeting with Group 2| Victoria, Yani, Daniel, Anton, Ivo|
| 9:40|  Meeting with Group 3| Rune and Rasmus|


### Monday ITelligence meeting at E-lab

What can you send to Martin and Nikolaj. Main point that needs to be addressed.

| Time | Activity |
| :---: | :--- |
| 10:00 |  Meeting with Group 1| Fadi Al-Salem Marius Tvarkunas Adam Dolinajec Papp Domonkos|
| 10:20 |  Meeting with Group 2| Adham Ahmad, Camilla Johannsen Kock, Jacob Back Ipsen, Jonas Nissen, Martin Banov |


### Monday Data Center meeting at N-Lab

| Time | Activity |
| :---: | :--- |
| 09:00 |All teams|
| 12:15 |End of meeting. Update gitlab|

### End of Day

Students will have everything set up and committed by the end of the day.

## Hands-on time

Students are welcomed to work outside regular hours and work/Develop further on their tasks

## Comments
New project goals and project plan will be posted tomorrow as been updated from companies.

