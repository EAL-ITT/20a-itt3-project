---
Week: 48
Content:  Joy Presemtation. Milistones
Material: See links in weekly plan
Initials: RUTR/ILES
---

# Week 48 updated

## Goals of the week(s)
Listening to presentation about PTI from Joy

### Practical goals
* Listen to presentations
* Look into milestones


### Learning goals
* Work Verification


## Deliverables

N/A

## Schedule

On the 23 of November, Joy Laue Christensen will come and present PIT top up at 09:00. It should take an hour. We continue team meetings afterwards. Presentation is at network lab.


## Meetings

Meetings will start at 10:15.


### Monday Vleppo meeting at E-Lab

Students need to create a block chain - centered case and topic. The idea is a brainstorm product that will be worked on and a concept developed.

| Time | Activity |
| :---: | :--- |
| 10:10 | All Teams meet at the ELab|
| 10:15 | Student present their block chain related topic|
| 11:00 | Break work further on topic after getting Jespers Feedback|


### Monday ITelligence meeting at E-lab

| Time | Activity |
| :---: | :--- |
| 11:00 | All Teams meet at the ELab|
| 11:05 | All teams present feedback from Martin|
| 11:30 | Outline of new gials and topic|


### Monday Data Center meeting at N-Lab

Please make sure you follow Hans Plan

| Time | Activity |
| :---: | :--- |
| 10:05 |All Data Center Teams: Follow Ilas Plan|


### End of Day

Students are to work on their project for the rest of the day

## Hands-on time

N/A

## Comments
* Project work
