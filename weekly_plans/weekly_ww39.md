---
Week: 39
Content:  milestones allocations
Material: See links in weekly plan
Initials: RUTR/ILES
---

# Week 39 updated

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals
* Meeting with students and supervisors
* Listening to Project status
* Looking into miles stones
* Jesper from Vleppo will be joining us for the meetings

### Learning goals
* Planning
* Allocation of resources


## Deliverables
* Resources and prototype outlook
* Quick and Dirty is the main focus
* Diagrams, and prototype


## Schedule

Below is the tentative schedule, which may be changed depending on input from the students.

### Monday Vleppo

| Time | Activity |
| :---: | :--- |
| 9:00 |  Meeting with Group 1 | Thomas, Barnabas, Jens and Gytis|
| 9:20 |  Meeting with Group 2| Victoria, Yani, Daniel, Anton, Ivo|
| 9:40|  Meeting with Group 3| Rune and Rasmus|


### Monday ITelligence

What can you send to Martin and Nikolaj. Main point that needs to be addressed.

| Time | Activity |
| :---: | :--- |
| 10:00 |  Meeting with Group 1| Fadi Al-Salem Marius Tvarkunas Adam Dolinajec Papp Domonkos|
| 10:20 |  Meeting with Group 2| Adham Ahmad, Camilla Johannsen Kock, Jacob Back Ipsen, Jonas Nissen, Martin Banov |


### Monday Data Center

| Time | Activity |
| :---: | :--- |
| 10:40 |  Meeting with Group 1| Ventilation |
| 11:00 |  Meeting with Group 2| Power/Electrical|
| 11:20| Meeting with Group 3| Cool/Heating|


### End of Day

Students will have everything set up and committed by the end of the day.

## Overview of technologies

How and if the Zigbee technologies apply to your project. Initial plan and overview.

## Hands-on time

Students are welcomed to work outside regular hours and work/Develop further on their tasks

## Comments
* This is an initial plan, it will be an agile project
* The weekly plans will be updated weekly according to team needs
