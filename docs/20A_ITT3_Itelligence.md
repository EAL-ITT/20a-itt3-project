---
title: '20A ITT3 Itelligence project'
filename: '20A_ITT3_Itelligence'
subtitle: 'Project description'
authors: ['Nikolaj Simonsen \<nisi@ucl.dk\>']
main_author: 'Nikolaj Simonsen'
date: 2020-06-22
email: 'nisi@ucl.dk'
left-header: \today
right-header: Project description
skip-toc: false
---

# Main Supervisor
NISI  
mail: nisi@ucl.dk  
element (riot): @npesdk:matrix.org 

# Itelligence


Itelligence is a company delivering consultancy within IT. They would like an "IoT visitor badge" that signals that they are a tech company to their visitors. The visitor badge has other possible scopes.

## Description from Itelligence:
"We imagine that students develop a prototype and an application where you can enter visitor name, scan a visitors company logo. We would like the badge to gadget like and also have a clock function that gives an alarm by changing the display ie. 15 minutes before the visitors parking ticket expire, we would like the students input on more gadget features"

Itelligence would like to have the students input on what is technically possible as well as possible methods of application.
Itelligence would like two groups of 4 students to work with the case to ensure as many as possible methods of application and different technical solutions. 

## Cooperation with Itelligence 
Itelligence will be present at project start to present the project, the company and how the company works with projects (with possible examples from previous projects).  
Itelligence will be available with answers to questions from student, this will be arranged through a google document.  
Itelligence would like to have the systems presented at project end, as well as access to the documentation of the projects through gitlab.
Counselor for this project at UCL is Nikolaj Simonsen.

## Features
The project will be divided into 3 phases, "proof of concept", "consolidation" and "make it useful for the user".

1. Proof of concept  
In this phase minimum functionality throughout the system is implemented. Example: Entering a visitor name in the application and showing it on the badge display.
2. Consolidation
The functionality is expanded to include "need to have" features. Along the "nice to have" features is identified and selected by the students.
3. Make it useful for the user
In this phase the documentation, tests, product and user manual are finished, preparing the project to be handed over to Itelligence. 

  
## Itelligence would like to have these results:    

**Need to have features**

* The badge can read visitor data from an Itelligence system
* Show visitor data on an E-Ink display
* Show QR code containing visitor data, on the display
* Configurable RGB status LED
* Tools to update badge firmware
* Interface to configure the badge from an Itelligence system
* Pretty looking and functional product/prototype  

**Nice to have features**  
* Alarm shown when parking ticket expires, shown on the badge display
* Incognito mode, only show information on the badge when in specific physical zone (ie. R&D dept. etc.)
* Show nearest emergency exit in emergency situations
* Show which queue to join when boarding a plane
* QR code to use in the Itelligence company canteen
* Students input on features

**Test**  
Itelligence would like the following evaluated:  

* Battery lifetime when using bluetooth
* Battery lifetime when using wifi
* Badge weight with different hardware options (aim for as low as possible)
* Range when using WIFI
* Range when using bluetooth
* Performance and limitations of used components

Itelligence will supply the primary components (RPi etc.) and UCL supplies stock components (resistors etc.)

**Component list**  
* Raspberry pi Zero
* E-ink display
* ? Buttons
* ? LED's
* ? 3D printed casing
* Battery and charger

## Students preparation
This project is a classic IoT project including programming in python, possibly C (knowledge of higher level languages is a plus), Linux scripting, 3D design, soldering and calculating electronics components, git version control, gitlab project planning and documentation and testing (user and system/modules).  
The system uses wireless technologies and basic knowledge of networking is required.  
The above is not required at project start but the studen

# 20A-ITT3 Project Part (B)

Discription to follow from NISI

## Report Content:

* Students presents their IoT solution in form of a prototype 
* Students reflect on acquired new knowledge, skills and competencies in relation to the 
data center industry / Block chain industry / IoT industry
* Students present the relevance and application of the knowledge gained during week 43-47 with respect to their IoT solution their experienced communication between groups from different professions and interdisciplinary collaboration
* Documentation of their work and progress
* Workmanner: Team work and cooperation
* Milestones
* Technical considerations ts can choose to read about the subjects before project start.
