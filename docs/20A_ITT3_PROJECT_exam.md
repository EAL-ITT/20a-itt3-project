---
title: '20A ITT3 Project'
filename: '20A_ITT3_PROJECT_exam'
subtitle: 'ProjectB Exam'
authors: ['Ruslan Trifonov \<rutr@ucl.dk\>', 'Ilias Esmati \<iles@ucl.dk\>']
main_author: 'Ruslan Trifonov'
date: 2020-11-18
email: 'rutr@ucl.dk'
left-header: \today
right-header: ProjectB Exam
skip-toc: false
---

# Document content

This document describes the exam and hand-in requirements for Project B Exam

# Exam description

The exam is individual, presentation of work and defense of a submitted report.
Each student receives 15 minutes of presentation time and 10 minutes of questioning and grade voting.
The students presentation should focus on the individual work for the project as descriptive in the report.
Avoid presenting the same project 4 times - presentation should be reflecting student's work.
 
The exam takes place at Seebladsgade 1, 5000 Odense.  
Room number is announced at Wiseflow

# Report Submission

The submitted report is a group/team report, submitted via wiseflow. Students are expected to work together in a team to write the report. Furthermore the report should state clearly which student worked on which parts - including primary topic.

For the report format please verity curriculum for restrictions and [Report Template](https://eal-itt.gitlab.io/Internship/about/template/) for the structure of a good report.

# Grading

The students grade is a combination of report, work and presentation.  Grading is 7 grade scale.  The exam is with internal sensor. 

# Hand-in requirements

The hand-in is Group/ Team based. One person can submitted for the group. Students are expected to hand-in a report on wiseflow.   

Requirements for the report at given in the curriculum of the education found at [ucl website](https://www.ucl.dk/)

A Report Template with the minimum requirements is found [here](https://eal-itt.gitlab.io/Internship/about/template/)

# Exam dates

Announced via WiseFlow

# Hand-in dates

Announced via WiseFlow

# Additional information

See the [semester description](https://esdhweb.ucl.dk/D20-1394523.pdf?nocache=8ec27635-588b-4367-93c7-3b99fd3c0d29&_ga=2.266824398.1784206826.1601976395-975700373.1599562108)
