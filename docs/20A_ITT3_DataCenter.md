---
title: '20A ITT3 DataCenter project'
filename: '20A_ITT3_DataCenter'
subtitle: 'Project description'
authors: ['Per Dahlstrøm \<pda@ucl.dk\>']
main_author: 'Per Dahlstrøm'
date: 2020-06-22
email: 'pda@ucl.dk'
left-header: \today
right-header: project description
skip-toc: false
---

# Main Supervisor
Per Dahlstrøm  
mail: pda@ucl.dk  

## co-supervisors
Nikolaj Simonsen  
mail: nisi@ucl.dk  
element (riot): @npesdk:matrix.org 


Ilias Esmati  
mail: iles@ucl.dk  

# Case DataCenter

This project is a cooperation between various partners. UCL, Fredericia Maskinmesterskole, NNIT, Danish Data Center Industry, Data Center Grupper and Google. The main supervisor is PDA with reference to the project and the network part of it. With reference to the Electronics, Embedded and other parts the students are provided with assistance form NISI, ILES and RUTR.


## Description from Partners: As provided this case is still in Draft Version
"Start with the end in mind": It's something everyone agrees on, but it's always set aside for one reason or another (Typical Economy)

Datacenter A/S is in the process of establishing a new datacenter called Datacamp NOW for a customer.
Datacenter A/S is planning and developing the new datacenter to be launched on February 1, 2021.
In DataCamp NOW, a new data center unit with X number of servers (several specs missing) will be built,which will provide Tb space based on given quality goals. (More details are missing here)

* What kind of data center is this, housing, hosting or?
* If more customers in the same data center, are there any customers who want to have their own network in the center?
* Customer requirements for certifications for data center design and or operation? Should security be blended into this?
* Access control, etc.
* How to test the data center annually?

More information about Datacenter A/S will be given on a later data

* Data Center Specs
* Facility
* ICT Servers, Routers, Switches etc
* Energy needs
* Cooling and temperature requirements
* Humidity
* What if the data center demands that the cold temperature in the server room be 32,3 then you could run free-cooling mode all year round?
* Ashrae climate data
* Benefits for PUE

## Assignments for teams of students

Joint tasks FMS and UCL:
* Identify common interfaces for Operations and ICT teams in a data center?
* How should the internal network for utility be ?: Closed or open?
* Who has the responsibility for backup and operations?
* What value do the common interfaces have?
* Make a drawing of the common interfaces in a data center that everyone in Datacenter A/S and its customers can understand

FMS Maskinmester:
* Describe and select power and cooling equipment with regard to maintenance safety
needs, necessary functions economy and energy optimization
* Different cooling layout depending on customer requirements
* Some customers buy an entire room
* Some buy only one rack
* Layout vs risk vs PUE

* Set requirements for monitoring system in relation to the necessary data for use in
maintenance and safety monitoring
* How much to log and how often
* Who is in charge of storage
* Should the system be controlling, partially controlling or only for monitoring
* Make drawings of cooling electricity and ventilation ICT team tasks

Computer Science: Based on requirements
* an argumentative presentation is made for a monitoring system that collects and processes relevant cooling energy data from servers for security monitoring and energy optimization Consider the following
* Predictive maintenance
* Use of historical data as a regulatory parameter

IT Technology:
* Based on relevant research, a network configuration proposal is developed
based on the given specs. Consider Hypervisor and virtualization in the presentation.
* Martin will try to get in touch with a colleague from NNIT, who works with network
technologies for further development of the network configuration.

## MidTerm
Mid term presentations are done internally to the Facility/ICT teams with critical feedback. Next, The teams will make final presentations for the management of Datacenter 
A/S, who provides final feedback and possibly final approval.

## Learning objectives

Knowledge | The student has knowledge about:
* understanding of other professional groups practices in relation to the data center industry
* the value creating interaction between disciplines in relation to the data center industry
* development-based knowledge of the industry's applied practice, theory and methodology
within technology development
* basic business understanding in relation to the data center industry

Skills | The student can:
* Evaluate issues and develop prototype solutions in relation to the chosen topics
* Communicate key results

Competences
* participate in academic and interdisciplinary collaboration and project work in connection with
technology development with a professional approach, and participate in the development of
practice within technology development
* in a structured context, acquire new knowledge, skills and competences in relation to the data center industry, including domain knowledge and technological know-how and the use of new ones methods, techniques and tools.
* Participate in practical development processes
* Manage customer assignments in order to translate customer needs into solutions



# 20A-ITT3 Project Part (B)

Implementation of  IoT systems in UCL N-LAB (Server Room).


## Objectives

Students implement IoT systems in UCL N-LAB (Server Room) based on what they have designed before week 41. 

## Report Content:

* Students presents their IoT solution in form of a prototype 
* Students reflect on acquired new knowledge, skills and competencies in relation to the 
data center industry / Block chain industry / IoT industry
* Students present the relevance and application of the knowledge gained during week 43-47 with respect to their IoT solution their experienced communication between groups from different professions and interdisciplinary collaboration
* Documentation of their work and progress
* Workmanner: Team work and cooperation
* Milestones
* Technical considerations 

