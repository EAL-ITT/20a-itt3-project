---
title: '20A ITT3 Vleppo project'
filename: '20A_ITT3_Vleppo'
subtitle: 'Project description'
authors: ['Ruslan Trifonov \<rutr@ucl.dk\>', 'Ilias Esmati \<iles@ucl.dk\>']
main_author: 'Ruslan Trifonov'
date: 2020-07-06
email: 'rutr@ucl.dk'
left-header: \today
right-header: project description
skip-toc: false
---

# Main Supervisor 
Ruslan Trifonov  
mail: rutr@ucl.dk    

# Vleppo

Vleppo is a software development company. That develops secure payment solutions, secure file transactions and secure digital assets. They are based in Odense. They would like to have 2 to 3 groups of 3 to 4 students developing their next generation produces.

## Description from Vleppo:
Develop a weather station (or other type of periodic data collection) system that records measurements to a blockchain using the Komodo Platform. The measurements should be viewable on an external device (preferably a web server) that is also synced to the same blockchain.

Raw objectives as given by Vleppo:

1. Build and test a proof of concept for an ARM-based device that periodically collects data (e.g. a weather station).
2. Build and test a proof of concept for uploading arbitrary data to a Komodo-based blockchain using an ARM-based device. (for minimum required hardware specs see here: https://docs.komodoplatform.com/basic-docs/smart-chains/smart-chain-setup/installing-from-source.html)
3. Design, build and test a system containing at least two ARM-based blockchain nodes that periodically upload measurement data to a common Komodo-based blockchain.
    4. Design, build and test a blockchain explorer node that can retrieve the data uploaded by the system nodes and process it in some manner. (for example, if the system nodes collect temperature, moisture and other similar data, then the node should be able to calculate average weekly/monthly values)

## Cooperation with Vleppo
The company will provide a lead developer that the students will have access to. The control of the project will be done via gitlab. Responsible for the project from UCL side will be, Ruslan Trifonov.

## Objectives for the project  
The project needs to run for a semester and the students are free to add to the solutions. Programming wise, the students need to focus on secure code. Optimization is high on the agenda.

1. The device of implementation needs to be ARM based. For example RPI.
2. Using blockchains(Komodo) the data collected will be uploaded on a website/cloud/database solution.
3. The students should build a network of nodes to collect data. The data will be centralized, using point (2) method.
4. Data should be ready to be processed via a gui. The basic, time stamp format can be used.



Objective will change after the first meeting with the company. But the overall project will not.


## Students preparation
This project is heavy based around, programming and scripting on Linux. C/C++ and Python are needed. GUI design with QT will be also used. Blockchain knowledge and using is further more required. Project will be controlled via git version control, gitlab project planning and documentation and testing (user and system/modules). The system has the potential to be a product and the student has internship/job opportunities with Vleppo.

## Other general information
None at this time.



# 20A-ITT3 Project Part (B)

The current status for what we call Project Sol is that the interest of having a household device capable of monitoring and assisting owners to sell/buy excess green energy is increasing. We currently have receive positive feedback from both American and Australian companies in our current ideas for the project.



## Tasks

* Research and development, compile a list of what you would find innovative in this market. Is there some specific sensors, calculations that might improve on a system like this.

* As a group agree on three of the ideas, and try to explore it further. It would be optimal to try and develop a proof of concept to support your project idea. Remember the KISS model!

* Create a small pitch for the idea, that you can present with the included proof of concept for each of them. Remember to try and consider what you as a group would recommend to implement and develop.

* When you have presented and one of the ideas have been approved moved into the development phase.

## Report Content:

* Students presents their IoT solution in form of a prototype 
* Students reflect on acquired new knowledge, skills and competencies in relation to the 
data center industry / Block chain industry / IoT industry
* Students present the relevance and application of the knowledge gained during week 43-47 with respect to their IoT solution their experienced communication between groups from different professions and interdisciplinary collaboration
* Documentation of their work and progress
* Workmanner: Team work and cooperation
* Milestones
* Technical considerations ts can choose to read about the subjects before project start.




