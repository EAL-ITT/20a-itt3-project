---
title: '20A ITT3 PROJECT lecture plan'
filename: '20A_ITT3_PROJECT_lecture_plan'
subtitle: 'Lecture plan'
authors: ['Ruslan Trifonov \<rutr@ucl.dk\>', 'Ilias Esmati \<iles@ucl.dk\>']
main_author: 'Ruslan Trifonov'
date: 2020-06-22
email: 'rutr@ucl.dk'
left-header: \today
right-header: Lecture plan
skip-toc: false
---


# Lecture plan

The lecture plan consists of the following parts: a week/lecture plan, extra documentation for various projects. An overview of the distribution of study activities and general information about the 3.sem project.

* Study program, class and semester: IT technology, oeait19, 20A
* Name of lecturer and date of filling in form: RUTR 2019-06-22
* Title of the course, module or project and ECTS: Semester project, phase one and phase two. Each have 5 ECTS
* Required readings, literature or technical instructions and other background material:None
* Other requirement: There will be several project, each will have various programming languages. 

See weekly plan for details like detailed daily plan, links, references, exercises and so on. From week 36 until the end of the semester, RUTR and ILES will guide the students with reference to the project. A special adviser/supervisor/technical assistant will be provided for some of the projects.


| Teacher |  Week | Content |
| :---: | :---: | :--- |
|  RUTR/ILES | 35 | Introduction, and methodology|
|  RUTR/ILES/Special | 36 | Project form, milestones, goals as well as technical support|

# General info about the course, module or project

This course will be a total of 10 ECTS over two trimester. The first trimester is to be seen as phase one and the research, investigation or set up for the final out come of the project as defined. The second trimester is to be seen as the consecration, of knowledge. Implementation of the final version of software or hardware with focus the final outcome of the project. The projects have limited amount of seeds. Each group is allowed to have between 3 to 4 students. Further more each project can be allocated a mix between 2 to 4 groups.

## The student’s learning outcome

There are some common learning outcomes and some project specific. The general learning goals and outcome is given below, while the project specific is given together with the project descriptions. Overview of the projects with Itelligence, Vleppo and Danish Datacenters is given in separate documentations. Information and links to documentation is found on the 20A ITT3 PROJECT website under: Weekly plans, exercises and docs as pdf files


### General learning outcomes Knowledge


* The students will work with a real project assigned from a company.
* The students will learn to implement innovative methods used in problem solving.
* The students will be asked to project manage their own work. In order to learn how to structure and segment.
* The students will learn about business organizational and economy.
* The student will learn about quality and resources management/allocation.
* The students will learn to think as advisers in a consultancy function.


### General learning outcomes Skills

* Communication methods and control.
* Customer requirement and problem solving



### General learning outcomes Competencies

* Requirements to specifications.
* Planning, managing and executing.
* Acquiring skills and knowledge that are essential but missing.

## Content

The course is formed around a project, and will include relevant technology from the curriculum. It is intended for cross-topic consolidation. A team can not focus online on one aspect of the curriculum. But individual students can focus on specific topics.

## Method

The project will be divided into blocks that build upon each other. Students have tasks given by the companies/lectures and they will have to segment and work separately on each point. Yet they have to keep close contract with the other group member and the company.

## Equipment

If needed given in the project descriptions.

## Projects with external collaborators  (proportion of students who participated)
Given in the project descriptions.

## Test form/assessment
The project includes a written report and an oral defense of the report. The report has to follow the guidelines from the curriculum as well as the advice from the project supervisors.

See exam catalogue for details on compulsory elements.

## Project Descriptions

There are total of 6 project currently. Some of the them are all ready arranged some of them are been currently worked on. Here all the currently active project are given.

### Itelligence - NISI
Itelligence is a company delivering consultancy within IT. They would like an "IoT visitor badge" that signals that they are a tech company to their visitors. The visitor badge has other possible scopes.

#### Description from Itelligence:
"We imagine that students develop a prototype and an application where you can enter visitor name, scan a visitors company logo. We would like the badge to gadget like and also have a clock function that gives an alarm by changing the display ie. 15 minutes before the visitors parking ticket expire, we would like the students input on more gadget features"

Itelligence would like to have the students input on what is technically possible as well as possible methods of application.
Itelligence would like two groups of 4 students to work with the case to ensure as many as possible methods of application and different technical solutions. 

#### Cooperation with Itelligence 
Itelligence will be present at project start to present the project, the company and how the company works with projects (with possible examples from previous projects).  
Itelligence will be available with answers to questions from student, this will be arranged through a google document.  
Itelligence would like to have the systems presented at project end, as well as access to the documentation of the projects through gitlab.
Counselor for this project at UCL is Nikolaj Simonsen.

#### Features
The project will be divided into 3 phases, "proof of concept", "consolidation" and "make it useful for the user".

1. Proof of concept  
In this phase minimum functionality throughout the system is implemented. Example: Entering a visitor name in the application and showing it on the badge display.
2. Consolidation
The functionality is expanded to include "need to have" features. Along the "nice to have" features is identified and selected by the students.
3. Make it useful for the user
In this phase the documentation, tests, product and user manual are finished, preparing the project to be handed over to Itelligence. 

  
#### Itelligence would like to have these results:    

**Need to have features**

* The badge can read visitor data from an Itelligence system
* Show visitor data on an E-Ink display
* Show QR code containing visitor data, on the display
* Configurable RGB status LED
* Tools to update badge firmware
* Interface to configure the badge from an Itelligence system
* Pretty looking and functional product/prototype  

**Nice to have features**  
* Alarm shown when parking ticket expires, shown on the badge display
* Incognito mode, only show information on the badge when in specific physical zone (ie. R&D dept. etc.)
* Show nearest emergency exit in emergency situations
* Show which queue to join when boarding a plane
* QR code to use in the Itelligence company canteen
* Students input on features

**Test**  
Itelligence would like the following evaluated:  

* Battery lifetime when using bluetooth
* Battery lifetime when using wifi
* Badge weight with different hardware options (aim for as low as possible)
* Range when using WIFI
* Range when using bluetooth
* Performance and limitations of used components

Itelligence will supply the primary components (RPi etc.) and UCL supplies stock components (resistors etc.)

**Component list**  
* Raspberry pi Zero
* E-ink display
* ? Buttons
* ? LED's
* ? 3D printed casing
* Battery and charger

#### Students preparation
This project is a classic IoT project including programming in python, possibly C (knowledge of higher level languages is a plus), Linux scripting, 3D design, soldering and calculating electronics components, git version control, gitlab project planning and documentation and testing (user and system/modules).  
The system uses wireless technologies and basic knowledge of networking is required.  
The above is not required at project start but the students can choose to read about the subjects before project start.



### Vleppo - RUTR
Vleppo is a software development company. That develops secure payment solutions, secure file transactions and secure digital assets. They are based in Odense. They would like to have 2 to 3 groups of 3 to 4 students developing their next generation produces.

#### Description from Vleppo:
Develop a weather station (or other type of periodic data collection) system that records measurements to a blockchain using the Komodo Platform. The measurements should be viewable on an external device (preferably a web server) that is also synced to the same blockchain.

Raw objectives as given by Vleppo:

1. Build and test a proof of concept for an ARM-based device that periodically collects data (e.g. a weather station).
2. Build and test a proof of concept for uploading arbitrary data to a Komodo-based blockchain using an ARM-based device. (for minimum required hardware specs see here: https://docs.komodoplatform.com/basic-docs/smart-chains/smart-chain-setup/installing-from-source.html)
3. Design, build and test a system containing at least two ARM-based blockchain nodes that periodically upload measurement data to a common Komodo-based blockchain.
    4. Design, build and test a blockchain explorer node that can retrieve the data uploaded by the system nodes and process it in some manner. (for example, if the system nodes collect temperature, moisture and other similar data, then the node should be able to calculate average weekly/monthly values)

#### Cooperation with Vleppo
The company will provide a lead developer that the students will have access to. The control of the project will be done via gitlab. Responsible for the project from UCL side will be, Ruslan Trifonov.

#### Objectives for the project  
The project needs to run for a semester and the students are free to add to the solutions. Programming wise, the students need to focus on secure code. Optimization is high on the agenda.

1. The device of implementation needs to be ARM based. For example RPI.
2. Using blockchains(Komodo) the data collected will be uploaded on a website/cloud/database solution.
3. The students should build a network of nodes to collect data. The data will be centralized, using point (2) method.
4. Data should be ready to be processed via a gui. The basic, time stamp format can be used.



Objective will change after the first meeting with the company. But the overall project will not.



#### Students preparation
This project is heavy based around, programming and scripting on Linux. C/C++ and Python are needed. GUI design with QT will be also used. Blockchain knowledge and using is further more required. Project will be controlled via git version control, gitlab project planning and documentation and testing (user and system/modules). The system has the potential to be a product and the student has internship/job opportunities with Vleppo.

## Other general information
None at this time.
