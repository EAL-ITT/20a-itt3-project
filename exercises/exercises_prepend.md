---
title: '20A ITT3 PROJECT'
subtitle: 'Exercises'
authors: ['Ruslan Trifonov \<nisi@ucl.dk\>', 'Ilias Esmati \<iles@ucl.dk\>']
main_author: 'Ruslan Trifonov'
date: \today
email: 'rutr@ucl.dk'
left-header: \today
right-header: '20A ITT3 PROJECT, exercises'
---


Introduction
====================

This document is a collection of exercises. They are associated with the weekly plans. 

References

* [Weekly plans](https://eal-itt.gitlab.io/20a-itt3-project/20A_ITT3_PROJECT_weekly_plans.pdf)



